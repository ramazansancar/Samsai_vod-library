use warp::Filter;

use tera::{Context, Tera};

use percent_encoding;

use std::sync::Arc;

use std::path::{PathBuf};
use std::collections::HashMap;

use std::env;
use std::fs;

fn render_vod_list(files: Vec<String>, tera: Arc<Tera>) -> impl warp::Reply {
    let mut context = Context::new();
    let vod_path = env::var("VOD_PATH").expect("VOD_PATH not set!");
    let mut total_size = 0;
    let mut file_info: HashMap<String, String> = HashMap::new();
    for file in &files {
        let file_path = format!("{}/{}", vod_path, file);
        //println!("{}", file_path);
        let metadata = fs::metadata(file_path).unwrap();
        if metadata.is_dir() {
            continue;
        }
        let file_size = metadata.len();
        let file_size_str = format_size(file_size);

        file_info.insert(file.to_owned(), file_size_str);
        // Total Size Calculation
        total_size += file_size;
    }
    
    context.insert("files", &file_info);
    context.insert("total_size", &format_size(total_size));

    warp::reply::html(tera.render("index.html", &context).unwrap())
}


fn format_size(size: u64) -> String {
    let kb = 1024;
    let mb = kb * 1024;
    let gb = mb * 1024;

    if size < kb {
        format!("{} B", size)
    } else if size < mb {
        format!("{:.2} KB", size as f64 / kb as f64)
    } else if size < gb {
        format!("{:.2} MB", size as f64 / mb as f64)
    } else {
        format!("{:.2} GB", size as f64 / gb as f64)
    }
}

fn render_vod_view(vod_file: String, tera: Arc<Tera>) -> impl warp::Reply {
    let mut context = Context::new();
    context.insert("vod", &vod_file);

    warp::reply::html(tera.render("vod.html", &context).unwrap())
}

fn get_vod_files(path: &std::path::Path) -> Vec<String> {
    let mut entries: Vec<String> = fs::read_dir(path)
        .unwrap()
        .filter(|entry| entry.is_ok())
        .map(|entry| entry.unwrap())
        .map(|entry| {
            entry
                .file_name()
                .into_string()
                .expect("Failed to convert from OsStr")
        })
        .collect();

    entries.sort_by(|a: &String, b: &String| b.cmp(&a));

    entries
}

#[tokio::main]
async fn main() {
    let vod_path = Arc::new(std::path::PathBuf::from(
        env::var("VOD_PATH").expect("VOD_PATH not set!"),
    ));

    let port = env::var("PORT").unwrap_or(String::from("3030"));

    let tera = match Tera::new("templates/**/*.html") {
        Ok(t) => Arc::new(t),
        Err(e) => {
            println!("Parsing error(s): {}", e);
            ::std::process::exit(1);
        }
    };

    let vod_path_for_list = vod_path.clone();

    let tera_for_list = tera.clone();
    let vod_list = warp::path::end()
        .map(move || get_vod_files(&vod_path_for_list.clone()))
        .map(move |vods| render_vod_list(vods, tera_for_list.clone()));

    //let _vod_path_for_view = vod_path.clone();

    let tera_for_view = tera.clone();
    let vod_view = warp::path!("vod" / String)
        .map(move |file: String| {
            String::from(percent_encoding::percent_decode_str(&file).decode_utf8_lossy())
        })
        .map(move |vod_file: String| render_vod_view(vod_file, tera_for_view.clone()));

    let files_route = warp::path("files").and(warp::fs::dir(PathBuf::from(vod_path.as_path())));

    println!("Yes, it is running. Don't worry.");
    println!("Listening on port http://127.0.0.1:{}", port);

    let routes = warp::get().and(vod_list.or(vod_view).or(files_route));

    warp::serve(routes)
        .run(([127, 0, 0, 1], port.parse().unwrap()))
        .await;
}

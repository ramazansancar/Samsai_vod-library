#!/usr/bin/env bash
set -euo pipefail

cargo build --release

mkdir -p build
cp target/release/vod-library build/
chmod +x build/vod-library
cp -r templates build/

rsync -r -a -v -e ssh --delete-after build root@samsai.eu:/home/vod-library/vod-library
ssh root@samsai.eu chown vod-library /home/vod-library/vod-library
